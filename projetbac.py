#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 18:15:31 2019

@author: utilisateur
"""

score=0
nombre_de_marches_a_gravir=0

ensemble_de_questions_reponses = [
        {"Question":"Qui à écrit Phèdre",
         "Reponses": {
                "Bonne": "Racine",
                "Mauvaises": ["Molière",
                              "Rousseau",
                              "Jean Luc Mélanchon"]
                }
        },
        {"Question":"Qui a gagné la CDM de football du Brésil en 2014",
         "Reponses": {
                "Bonne": "Allemagne",
                "Mauvaises": ["Argentine",
                              "Italie",
                              "France"]
                }
        },
        {"Question":"Quel est le nom du héros de la saga Zelda",
         "Reponses": {
                "Bonne": "Link",
                "Mauvaises": ["Zelda",
                              "Sheik",
                              "Impa"]
                }
        },
         {"Question":"Qui s'est déclaré \"premier flic de France\"",
         "Reponses": {
                "Bonne": "Georges Clemenceau",
                "Mauvaises": ["Léon Blum",
                              "Manuel Valls",
                              "Charles De Gaulles"]
                }
        },
         {"Question":"Quel acteur américain incarne le héros du film de Chistopher Nolan de 2014 : \"Interstellar\"",
         "Reponses": {
                "Bonne": "Mattew MacConaughey",
                "Mauvaises": ["Tom Cuise",
                              "Léonardo Di Caprio",
                              "Brad Pitt"]
                }
        },
         {"Question":"Quel journal a été attaqué par des terroristes islamistes en janvier 2015",
         "Reponses": {
                "Bonne": "Charlie Hebdo",
                "Mauvaises": ["Libération",
                              "Le Monde",
                              "New York Times"]
                }
        },
         {"Question":"Quel animal andin de la famille des camélidés est élevé pour sa laine",
         "Reponses": {
                "Bonne": "Lama",
                "Mauvaises": ["Yak",
                              "Chameau",
                              "Buffle"]
                }
        },
         {"Question":"Quel pays a décidé par référendum de quitterl'Union Européenne",
         "Reponses": {
                "Bonne": "Royaume-Uni",
                "Mauvaises": ["Islande",
                              "Suède",
                              "Allemagne"]
                }
        },{"Question":"Quel célèbre dictateur dirigea l'URSS de 1920 à 1953",
         "Reponses": {
                "Bonne": "Staline",
                "Mauvaises": ["Lénine",
                              "Poutine",
                              "Trotski"]
                }
        },
         {"Question":"Dans quel pays peut-on retrouver la Catalogne, l'Andalousie et la Castille",
         "Reponses": {
                "Bonne": "Espagne",
                "Mauvaises": ["France",
                              "Danemark",
                              "Chine"]
                }
        },
         {"Question":"Qui a dit \"Alea jacta est\" (Le sort est jeté)",
         "Reponses": {
                "Bonne": "Jules Ceaser",
                "Mauvaises": ["Hitler",
                              "Napoléon",
                              "Staline"]
                }
        },
         {"Question":"A qui doit-on la musique : \"I shot the sheriff\"",
         "Reponses": {
                "Bonne": "Bob Marley",
                "Mauvaises": ["Eric Clapton",
                              "UB40",
                              "Jim Morrison"]
                }
        },
         {"Question":"Dans quel ville italienne se situe l'action de la pièce de théätre de Shakespeare : \"Roméo and Juliett\"",
         "Reponses": {
                "Bonne": "Vérone",
                "Mauvaises": ["Rome",
                              "Milan",
                              "Venise"]
                }
        },
         {"Question":"Qui était le Dieu grec de la guerre",
         "Reponses": {
                "Bonne": "Arès",
                "Mauvaises": ["Apollon",
                              "Athéna",
                              "Hadès"]
                }
        },
         {"Question":"Quel es l'impératif du verbe feindre à la 1ère personne du pluriel",
         "Reponses": {
                "Bonne": "Feignons",
                "Mauvaises": ["Feins",
                              "Feignez",
                              "Feindons"]
                }
        },
         {"Question":"Quel roi de France proclama l'Edit de Nantes",
         "Reponses": {
                "Bonne": "Henry IV",
                "Mauvaises": ["Louis XVI",
                              "François Ier",
                              "Saint Louis"]
                }
        },
         {"Question":"Qui a écrit \"L'Illiade et l'Odyssée\"",
         "Reponses": {
                "Bonne": "Homère",
                "Mauvaises": ["Virgile",
                              "Euripide",
                              "Sophocle"]
                }
        },
         {"Question":"Quel est la racine carré de 9",
         "Reponses": {
                "Bonne": "3",
                "Mauvaises": ["6",
                              "81",
                              "2"]
                }
        },
         {"Question":"Quelle ville est la capitale des Etats-Unis",
         "Reponses": {
                "Bonne": "Washington DC",
                "Mauvaises": ["New York City",
                              "Los Angeles",
                              "Atlanta"]
                }
        },
         {"Question":"Quel animal terrestre est le plus lourd",
         "Reponses": {
                "Bonne": "Eléphant",
                "Mauvaises": ["Fourmi",
                              "Girafe",
                              "Baleine"]
                }
        },
        {"Question":"Quel élément est le plus présent dans l'atmoshère",
         "Reponses": {
                "Bonne": "N",
                "Mauvaises": ["O",
                              "H2O",
                              "CO2"]
                }
        },
         {"Question":"Comment s'appelle la femelle du cheval",
         "Reponses": {
                "Bonne": "Jument",
                "Mauvaises": ["Chevalette",
                              "Poulain",
                              "Anesse"]
                }
        },
         {"Question":"Quel organe permet de faire des échanges gazeux chez les mammifères",
         "Reponses": {
                "Bonne": "Poumons",
                "Mauvaises": ["Branchies",
                              "Nez",
                              "Coeur"]
                }
        },
         {"Question":"Qui a écrit \"Mein Kampf\"",
         "Reponses": {
                "Bonne": "Hitler",
                "Mauvaises": ["Trump",
                              "Justin Bieber",
                              "Hannibal"]
                }
        },
         {"Question":"D'où vient Sacha dans Pokemon",
         "Reponses": {
                "Bonne": "Bourg Palette",
                "Mauvaises": ["Unionpolis",
                              "Poketown",
                              "Paris"]
                }
        },
         {"Question":"Quel est le 1er Homme à avoir touché la Lune",
         "Reponses": {
                "Bonne": "Amstrong",
                "Mauvaises": ["Pesquet",
                              "Poutine",
                              "Gus"]
                }
        },
         {"Question":"What is the french for \"jail\"",
         "Reponses": {
                "Bonne": "Prison",
                "Mauvaises": ["Gel",
                              "Bus",
                              "Volant"]
                }
        },
         {"Question":"Quel est l'anglais de \"radio\"",
         "Reponses": {
                "Bonne": "radio",
                "Mauvaises": ["Gaga",
                              "radeau",
                              "rideau"]
                }
        },
         {"Question":"Qui a écrit \"Regarde les lumières,mon amour",
         "Reponses": {
                "Bonne": "Annie Ernaux",
                "Mauvaises": ["Victor Hugo",
                              "Marguerite Yourcenar",
                              "Luke Skywalker"]
                }
        }
        ]
         
nombre_de_marches_a_gravir=int(input("Choississez le nombre de marches que vous voulez gravir (max {})! :".format(len(ensemble_de_questions_reponses))))
"""
while nombre_de_marches_a_gravir != int(len(ensemble_de_questions_reponses)):
    if nombre_de_marches_a_gravir <= int(len(ensemble_de_questions_reponses)):
        print("C'est parti pour l'ascension de", nombre_de_marches_a_gravir, "marches")
    else:
        print("un chiffre entre 1 et {} s'il vous plaît !".format(len(ensemble_de_questions_reponses)))
        nombre_de_marches_a_gravir=int(input("Choississez le nombre de marches que vous voulez gravir (max {})! :".format(len(ensemble_de_questions_reponses)))) 
"""
print("C'est parti pour l'ascension de", nombre_de_marches_a_gravir, "marches")
print()

import random 
random.shuffle(ensemble_de_questions_reponses)

for nombre_de_marches in range (0,nombre_de_marches_a_gravir):
    print("La question est :")
    print(ensemble_de_questions_reponses[0]["Question"], "?")
    print()
    print("Les réponses possibles sont :")
    
    reponses_possibles = [ensemble_de_questions_reponses[0]["Reponses"]["Bonne"]]
    reponses_possibles.extend(ensemble_de_questions_reponses[0]["Reponses"]["Mauvaises"])
    random.shuffle(reponses_possibles)
    
    numero_reponse = 1
    for reponse in reponses_possibles:
        print("{}: {}".format(numero_reponse, reponse))
        numero_reponse = numero_reponse + 1
    
    
    reponse_utilisateur = -1 + int(input("Quelle est votre réponse (appuyer sur le chiffre de la bonne réponse) :" ))
    
    #while reponse_utilisateur != int(len(reponses_possibles)):
        #print("Vous ne pouvez entrer qu'un chiffre entre 1 et 4")
        #reponse_utilisateur = -1 + int(input("Quelle est votre réponse (appuyer sur le chiffre de la bonne réponse) :" ))
        
    if reponses_possibles[reponse_utilisateur] == ensemble_de_questions_reponses[0]["Reponses"]["Bonne"]:
        print("Gagné")
        score = score + 1
    else:
        print("Perdu")
    print()
    
    ensemble_de_questions_reponses.pop(0)
    
print("Votre score est :")
print(score)
