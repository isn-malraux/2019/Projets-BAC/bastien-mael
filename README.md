# Les Marches de l’Empereur

## CONTEXTE
Dans le cas du projet-bac dans le cours d’informatique et des sciences
du numériques, nous élèves de terminales nous avons le devoir
d’élaborer un programme.

Ce programme résultera d’un jeu qui a pour but que deux joueurs
s’affrontent simultanément sur des ordinateurs différents. Dans ce
jeu, il est question de gravir des marches pour arriver le premier en
haut de celles-ci. Pour monter une marche il faudra répondre
correctement à une question de culture générale dans un temps limité.

Par conséquent, si le joueur a enchaîné les bonnes réponses, il se
verra recevoir un bonus, à contrario celui qui enchaîne les mauvaises
réponses aura un malus.

Nous allons utiliser les logiciel suivant : framagit, python, discord,
terminal, web

## Participants
 - Maël (réseau)
 - Bastien (moteur)

## Planing
- 26/04 ou 03/05(3 semaines de cour) terminer le réseau et le programme
- 24/05 tout est fini 
